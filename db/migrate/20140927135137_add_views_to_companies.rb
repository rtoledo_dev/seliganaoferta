class AddViewsToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :views, :integer
  end
end
