class AddSlugToProductAds < ActiveRecord::Migration
  def change
    add_column :product_ads, :slug, :string
    add_index :product_ads, :slug, unique: true
  end
end
