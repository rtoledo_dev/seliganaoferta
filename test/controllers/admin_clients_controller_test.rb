require 'test_helper'

class AdminClientsControllerTest < ActionController::TestCase
  setup do
    @admin_client = admin_clients(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_clients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_client" do
    assert_difference('AdminClient.count') do
      post :create, admin_client: { address: @admin_client.address, address_city: @admin_client.address_city, address_neighborhood: @admin_client.address_neighborhood, address_state: @admin_client.address_state, address_zipcode: @admin_client.address_zipcode, agree: @admin_client.agree, city_id: @admin_client.city_id, company_id: @admin_client.company_id, cpf: @admin_client.cpf, email: @admin_client.email, mobile: @admin_client.mobile, name: @admin_client.name, newsletter: @admin_client.newsletter, phone: @admin_client.phone, rg: @admin_client.rg, site_term_id: @admin_client.site_term_id }
    end

    assert_redirected_to admin_client_path(assigns(:admin_client))
  end

  test "should show admin_client" do
    get :show, id: @admin_client
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_client
    assert_response :success
  end

  test "should update admin_client" do
    patch :update, id: @admin_client, admin_client: { address: @admin_client.address, address_city: @admin_client.address_city, address_neighborhood: @admin_client.address_neighborhood, address_state: @admin_client.address_state, address_zipcode: @admin_client.address_zipcode, agree: @admin_client.agree, city_id: @admin_client.city_id, company_id: @admin_client.company_id, cpf: @admin_client.cpf, email: @admin_client.email, mobile: @admin_client.mobile, name: @admin_client.name, newsletter: @admin_client.newsletter, phone: @admin_client.phone, rg: @admin_client.rg, site_term_id: @admin_client.site_term_id }
    assert_redirected_to admin_client_path(assigns(:admin_client))
  end

  test "should destroy admin_client" do
    assert_difference('AdminClient.count', -1) do
      delete :destroy, id: @admin_client
    end

    assert_redirected_to admin_clients_path
  end
end
