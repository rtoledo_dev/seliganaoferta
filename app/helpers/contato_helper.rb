module ContatoHelper
  def contact_subject_options
    [
      'Comercial', 'Financeiro', 'Contato informal', 'Outros assuntos'
    ]
  end
end
