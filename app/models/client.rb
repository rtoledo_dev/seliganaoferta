class Client < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :city
  belongs_to :company
  belongs_to :site_term

  before_create :set_mandatory_attributes

  def set_mandatory_attributes
    self.site_term_id = SiteTerm.last.id
    self.agree = true
  end

  def documents
    [cpf,rg].reject{|t| t.blank?}
  end

  def phones
    [mobile,phone].reject{|t| t.blank?}
  end
end
