class ProductAdImage < ActiveRecord::Base
  belongs_to :product_ad
  has_attached_file :image, styles: {thumb: '76x76#', big: '600', box: '215x215#'}, convert_options: {big: "-quality 90 -strip" }
  do_not_validate_attachment_file_type :image
end
