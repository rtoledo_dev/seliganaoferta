class PainelControleController < ApplicationController
  before_action :redirect_unless_company

  def index
    @active_product_ads = ProductAd.actives.where(company_id: current_client.company_id).all.sort{|t| t.total_coupons}.reverse
    @expired_product_ads = ProductAd.expireds.where(company_id: current_client.company_id).order(:end_use_at, :start_use_at)
  end

  def administrar_oferta
    @product_ad = ProductAd.friendly.find(params[:id])
  end

  def utilizar_cupon
    @product_ad_order = current_client.company.product_ad_orders.where(coupon: params[:id]).first
    if @product_ad_order.use_coupon
      flash[:success] = 'Cupom marcado como utilizado com sucesso'
    end
    redirect_to :back
  end

  def redirect_unless_company
    unless company_signed_in?
      flash[:error] = 'Você não tem acesso a esta área. Entre em contato conosco para se tornar um anunciante.'
      redirect_to contato_path
    end
  end
end
